//
// Created by frann on 30/11/2019.
//

#include "hello.h"
#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_lab6_MainActivity_helloFromJni(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from JNI";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_lab6_MainActivity_sum(
        JNIEnv *env,
        jobject,
        jint a,
        jint b
        ){
    return a+b;
}
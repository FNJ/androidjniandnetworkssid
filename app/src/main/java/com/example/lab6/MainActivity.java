package com.example.lab6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'hello' library on application startup.
    static {
        System.loadLibrary("hello");
    }

    /**
     * A native method that is implemented by the 'hello' native library,
     * which is packaged with this application.
     */
    public native String helloFromJni();
    public native int sum(int i, int j);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // This is click listener for button connect wifi
    public void connectWifi(View view){
        String networkSSID = "HotSpot - UI";
//        //if using password we can use it
//        String networkPass = "";

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = String.format("\"%s\"", networkSSID);

        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

//        //if using password we can use it
//        conf.wepKeys[0] = "\"" + networkPass + "\"";
//        conf.wepTxKeyIndex = 0;
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//        conf.preSharedKey = "\""+ networkPass +"\"";

        WifiManager wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
        int netId = wifiManager.addNetwork(conf);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
    }

    public void printHelloFromC(View view){
        TextView views = (TextView) findViewById(R.id.resultButtonPressed);
        views.setText(helloFromJni());
    }
    public void printOneTwoThree(View view){
        int res1 = sum(1,2);
        int retval = sum(res1, 3);
        TextView views = (TextView) findViewById(R.id.resultButtonPressed);
        views.setText(Integer.toString(retval));
    }
}
